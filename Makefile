ACPI_DUMP_DATA_PATH ?= ../acpi_dump

compile:
	iasl -tc dsdt.dsl

# NOTE: The decompiled table will be in $(ACPI_DUMP_DATA_PATH)/dsdt.dsl
decompile:
	[ -d $(ACPI_DUMP_DATA_PATH) ] && \
	  ( cd $(ACPI_DUMP_DATA_PATH); iasl -e ssdt*.dat -vt -d dsdt.dat; )

# Create an instrumented_initrd to be used like explained in the linux kernel
# docs (Documentation/acpi/initrd_table_override.txt).
initrd: compile
	[ -d kernel/firmware/acpi ] || mkdir -p kernel/firmware/acpi
	cp dsdt.aml kernel/firmware/acpi
	find kernel | cpio -H newc --create > instrumented_initrd

clean:
	rm -f *~ *.aml *.hex
